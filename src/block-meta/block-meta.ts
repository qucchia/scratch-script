import { Sb3Fields, Sb3Inputs } from "../sb3-block-data";

export type InputMeta = {
  input: string;
  inputType:
    | "string"
    | "number"
    | "naturalNumber"
    | "condition"
    | "stack"
    | "dropdown"
    | "message";
};

export type FieldMeta = {
  field: string;
  optionsMap?: { [key: string]: string };
};

export type ArgumentMeta =
  | { symbol: string }
  | { translate: string }
  | InputMeta
  | FieldMeta;

export type Layer =
  | {
      string: string;
      arguments?: ArgumentMeta[];
    }
  | { substack: string };

export type BlockType = "stack" | "stringNumberReporter" | "booleanReporter";

export class BlockMeta {
  opcode: string;
  type: BlockType;
  layers: Layer[];
  shapeEnd?: boolean;

  constructor({
    opcode,
    type,
    layers,
    shapeEnd = false,
  }: {
    opcode: string;
    type: BlockType;
    layers: Layer[];
    shapeEnd?: boolean;
  }) {
    this.opcode = opcode;
    this.type = type;
    this.layers = layers;
    this.shapeEnd = shapeEnd;
  }
}

export class BlockCategory {
  name: string;
  blocks: { [key: string]: BlockMeta };

  constructor({
    name,
    blocks,
  }: {
    name: string;
    blocks: { [opcode: string]: BlockMeta };
  }) {
    this.name = name;
    this.blocks = blocks;
  }
}

export class BlocksMeta {
  categories: { [key: string]: BlockCategory };

  constructor({
    categories,
  }: {
    categories: { [key: string]: BlockCategory };
  }) {
    this.categories = categories;
  }

  get(opcode: string) {
    const split = opcode.split("_");
    const category = this.categories[split[0]];
    if (!category) return;
    const block = category.blocks[split.slice(1).join("_")];
    return block;
  }
}

/*
export const blockArguments: { [key: string]: BlockArgument[] } = {
  control_if: [
    { input: "CONDITION", inputType: "condition" },
    { input: "SUBSTACK", inputType: "stack" },
  ],
  control_repeat: [
    { input: "TIMES", inputType: "naturalNumber" },
    { input: "SUBSTACK", inputType: "stack" },
  ],
  data_setvariableto: [
    { field: "VARIABLE" },
    { input: "VALUE", inputType: "string" },
  ],
  data_changevariableby: [
    { field: "VARIABLE" },
    { input: "VALUE", inputType: "string" },
  ],
  event_whenflagclicked: [{ symbol: "⚑" }],
  event_broadcastandwait: [{ input: "BROADCAST_INPUT", inputType: "message" }],
  looks_costume: [{ field: "COSTUME" }],
  looks_say: [{ input: "MESSAGE", inputType: "string" }],
  looks_switchcostumeto: [{ input: "COSTUME", inputType: "dropdown" }],
  looks_think: [{ input: "MESSAGE", inputType: "string" }],
  looks_thinkforsecs: [
    { input: "MESSAGE", inputType: "string" },
    { input: "SECS", inputType: "string" },
  ],
  motion_movesteps: [{ input: "STEPS", inputType: "string" }],
  motion_setrotationstyle: [
    {
      field: "STYLE",
      optionsMap: {
        "all around": "MOTION_SETROTATIONSTYLE_ALLAROUND",
        "left-right": "MOTION_SETROTATIONSTYLE_LEFTRIGHT",
        "don't rotate": "MOTION_SETROTATIONSTYLE_DONTROTATE",
      },
    },
  ],
  motion_turnright: [
    { symbol: "↻" },
    { input: "DEGREES", inputType: "string" },
  ],
  operator_gt: [
    { input: "OPERAND1", inputType: "string" },
    { input: "OPERAND2", inputType: "string" },
  ],
};
function layers(
  type: any,
  BlockType: any,
  layers: any,
  arg3: any,
  arg4: boolean
) {
  throw new Error("Function not implemented.");
}

*/

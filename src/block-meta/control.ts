import { BlockMeta, BlockCategory } from "./block-meta";

const control = new BlockCategory({
  name: "control",
  blocks: {
    forever: new BlockMeta({
      opcode: "forever",
      type: "stack",
      layers: [
        { string: "CONTROL_FOREVER" },
        { substack: "SUBSTACK" },
        { string: "END" },
      ],
      shapeEnd: true,
    }),
    repeat: new BlockMeta({
      opcode: "repeat",
      type: "stack",
      layers: [
        {
          string: "CONTROL_REPEAT",
          arguments: [{ input: "TIMES", inputType: "naturalNumber" }],
        },
        { substack: "SUBSTACK" },
        { string: "END", arguments: [] },
      ],
    }),
    if: new BlockMeta({
      opcode: "if",
      type: "stack",
      layers: [
        {
          string: "CONTROL_IF",
          arguments: [{ input: "CONDITION", inputType: "condition" }],
        },
        { substack: "SUBSTACK" },
        { string: "END", arguments: [] },
      ],
    }),
    if_else: {
      opcode: "if_else",
      type: "stack",
      layers: [
        {
          string: "CONTROL_IF",
          arguments: [{ input: "CONDITION", inputType: "condition" }],
        },
        { substack: "SUBSTACK" },
        { string: "CONTROL_ELSE", arguments: [] },
        { substack: "SUBSTACK2" },
        { string: "END", arguments: [] },
      ],
    },
    stop: {
      opcode: "stop",
      type: "stack",
      layers: [
        {
          string: "CONTROL_STOP",
          arguments: [{ field: "STOP_OPTION" }],
        },
      ],
    },
    wait: {
      opcode: "wait",
      type: "stack",
      layers: [
        {
          string: "CONTROL_WAIT",
          arguments: [{ input: "DURATION", inputType: "number" }], // TODO check type
        },
      ],
    },
  },
});

export default control;

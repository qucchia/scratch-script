import {
  BlockType,
  InputMeta,
  FieldMeta,
  ArgumentMeta,
  Layer,
  BlockMeta,
  BlocksMeta,
} from "./block-meta";
import control from "./control";

export default new BlocksMeta({
  categories: { control },
});

export {
  BlockType,
  InputMeta,
  FieldMeta,
  ArgumentMeta,
  Layer,
  BlockMeta,
  BlocksMeta,
};

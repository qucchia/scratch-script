import ScratchScript from "./scratch-script";
import fs from "fs";
import Translations from "./translate";

const blocks = JSON.parse(fs.readFileSync("example/project.json").toString())
  .targets[1].blocks;

const translations = new Translations(
  JSON.parse(fs.readFileSync("locales/es.json").toString())
);

console.log(
  new ScratchScript({
    blocks,
    translations,
  }).convert()
);

import { StringFormat } from "./string-formats";

export interface OptionsArgument {
  scratchblocks?: boolean;
  blockPositions?: boolean;
  stringFormat?:
    | StringFormat
    | [StringFormat, StringFormat?, StringFormat?, StringFormat?];
  indent?: string | number;
  relaxedNumbers?: boolean;
  relaxedDecimalPoints?: boolean;
  relaxedInputDropdowns?: boolean;
  relaxedFieldDropdowns?: boolean;
  useOperatorPrecedence?: boolean;
  curlyBracketStacks?: boolean;
}

export default class Options {
  scratchblocks: boolean;
  blockPositions: boolean;
  stringFormat: [StringFormat, StringFormat?, StringFormat?, StringFormat?];
  indent: string;
  relaxedNumbers: boolean;
  relaxedDecimalPoints: boolean;
  relaxedInputDropdowns: boolean;
  relaxedFieldDropdowns: boolean;
  useOperatorPrecedence: boolean;
  curlyBracketStacks: boolean;

  constructor(options: OptionsArgument) {
    this.scratchblocks =
      options.scratchblocks !== undefined ? options.scratchblocks : false;

    if (options.stringFormat !== undefined) {
      if (typeof options.stringFormat === "string") {
        this.stringFormat = [options.stringFormat];
      } else {
        this.stringFormat = options.stringFormat;
      }
    } else {
      if (this.scratchblocks) {
        this.stringFormat = ["squareBrackets"];
      } else {
        this.stringFormat = ["doubleQuotes", "singleQuotes", "doubleQuotes"];
      }
    }

    this.blockPositions =
      options.blockPositions !== undefined ? options.blockPositions : true;

    this.indent =
      options.indent !== undefined
        ? typeof options.indent === "number"
          ? " ".repeat(options.indent) // If indent option is a number, make indentation that many spaces
          : options.indent
        : "  "; // Default indent is 2 spaces

    this.relaxedNumbers =
      options.relaxedNumbers !== undefined
        ? options.relaxedNumbers
        : !this.scratchblocks;

    this.relaxedDecimalPoints =
      options.relaxedDecimalPoints !== undefined
        ? options.relaxedDecimalPoints
        : !this.scratchblocks;

    this.relaxedInputDropdowns =
      options.relaxedInputDropdowns !== undefined
        ? options.relaxedInputDropdowns
        : !this.scratchblocks;

    this.relaxedFieldDropdowns =
      options.relaxedFieldDropdowns !== undefined
        ? options.relaxedFieldDropdowns
        : !this.scratchblocks;

    this.useOperatorPrecedence =
      options.useOperatorPrecedence !== undefined
        ? options.useOperatorPrecedence
        : !this.scratchblocks;

    this.curlyBracketStacks =
      options.curlyBracketStacks !== undefined
        ? options.curlyBracketStacks
        : !this.scratchblocks;
  }
}

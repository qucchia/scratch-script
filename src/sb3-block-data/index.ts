export type Sb3NumberInput = [1, [4, string]];
export type Sb3NaturalNumberInput = [1, [6, string]];
export type Sb3StringInput = [1, [10, string]];
export type Sb3MessageInput = [1, [11, string]];

type Sb3PrimitiveInput =
  | Sb3NumberInput
  | Sb3NaturalNumberInput
  | Sb3StringInput
  | Sb3MessageInput;

export type Sb3DropdownReporterInput = [1, string /* Dropdown reporter ID */];
export type Sb3BlockInput = [2, string /* Block ID */];
export type Sb3VariableInput = [
  3,
  [12, string /* Variable name */, string /* Variable ID */],
  Sb3PrimitiveInput[1] /* Default value */
];
export type Sb3DropdownInput = [string /* Dropdown item value */, null];

export type Sb3Input =
  | Sb3PrimitiveInput
  | Sb3DropdownReporterInput
  | Sb3BlockInput
  | Sb3VariableInput
  | Sb3DropdownInput;

export type Sb3Inputs = { [key: string]: Sb3Input };

export type Sb3Field = [string, string | null];

export type Sb3Fields = { [key: string]: Sb3Field };

export type Sb3Block = {
  opcode: string;
  next: string | null;
  parent: string | null;
  inputs: {
    [key: string]: Sb3Input;
  };
  fields: {
    [key: string]: Sb3Field;
  };
  shadow: boolean;
  topLevel: boolean;
  x?: number;
  y?: number;
};

export type Sb3Blocks = { [key: string]: Sb3Block };

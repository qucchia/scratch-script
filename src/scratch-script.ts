import blocksMeta, {
  InputMeta,
  FieldMeta,
  Layer,
  ArgumentMeta,
} from "./block-meta";
import Options, { OptionsArgument } from "./options";
import {
  Sb3Input,
  Sb3Field,
  Sb3Block,
  Sb3Blocks,
  Sb3Inputs,
  Sb3Fields,
  Sb3BlockInput,
} from "./sb3-block-data";
import { stringFormats } from "./string-formats";
import Translations from "./translate";

export default class ScratchScript {
  blocks: Sb3Blocks;
  translations: Translations;
  options: Options;

  constructor({
    blocks,
    translations,
    options = {},
  }: {
    blocks: Sb3Blocks;
    translations: Translations;
    options?: OptionsArgument;
  }) {
    this.blocks = blocks;
    this.translations = translations;

    this.options = new Options(options);
  }

  convert() {
    const scripts: string[] = [];
    for (const blockId in this.blocks) {
      let block = this.blocks[blockId];
      if (block.topLevel) {
        let script = "";
        if (this.options.blockPositions) {
          script += `// @ ${block.x}, ${block.y}\n`;
        }
        script += this.convertBlock(block) + "\n";
        while (block.next !== null) {
          block = this.blocks[block.next];
          script += this.convertBlock(block) + "\n";
        }
        scripts.push(script);
      }
    }
    return scripts.join("\n");
  }

  getInput(arg: InputMeta, input: Sb3Input): string {
    switch (input[0]) {
      case 1:
        if (Array.isArray(input[1])) {
          const inputValue = input[1][1];
          if (input[1][0] === 11) {
            // Message
            return "(" + inputValue + " v)";
          } else {
            // Number or string
            let format = this.options.stringFormat[0];
            let formatMeta = stringFormats[format];

            for (const stringFormat of this.options.stringFormat.slice(1)) {
              if (stringFormat === undefined) break;
              if (!inputValue.includes(formatMeta.end)) break;
              format = stringFormat;
              formatMeta = stringFormats[format];
            }

            return (
              formatMeta.start +
              inputValue
                .replace(/\\/g, "\\\\") // Escape backslashes
                .replace(
                  // Escape string end character
                  new RegExp(formatMeta.end, "g"),
                  "\\" + formatMeta.end
                ) +
              formatMeta.end
            );
          }
        } else {
          // Dropdown reporter
          let block = this.blocks[input[1]];
          const field = Object.values(block.fields)[0][0];
          return "(" + field + " v)";
        }
      case 2:
        if (arg.inputType === "stack") {
          // Stack
          let block = this.blocks[input[1]];
          let blockText = "\n" + this.options.indent + this.convertBlock(block);
          while (block.next !== null) {
            block = this.blocks[block.next];
            blockText +=
              "\n" +
              this.convertBlock(block)
                .split("\n")
                .map((line) => this.options.indent + line)
                .join("\n");
          }
          return blockText;
        } else {
          // Reporter
          let block = this.blocks[input[1]];
          let blockText = this.convertBlock(block);
          return arg.inputType === "condition"
            ? `<${blockText}>`
            : `(${blockText})`;
        }
      case 3:
        return `(${input[1][1]})`;
      default:
        return "[unknown argument]";
    }
  }

  getField(arg: FieldMeta, field: Sb3Field) {
    const fieldValue = field[0];
    if (arg.optionsMap) {
      if (fieldValue in arg.optionsMap) {
        return (
          "[" + this.translations.translate(arg.optionsMap[fieldValue]) + " v]"
        );
      } else {
        return "[" + fieldValue + " v]";
      }
    } else {
      return "[" + fieldValue + " v]";
    }
  }

  convertArgument({
    argument,
    inputs,
    fields,
  }: {
    argument: ArgumentMeta;
    inputs: Sb3Inputs;
    fields: Sb3Fields;
  }) {
    if ("symbol" in argument) {
      // Symbol of green flag, turn clockwise, etc
      return argument.symbol;
    } else if ("input" in argument && argument.input in inputs) {
      return this.getInput(argument, inputs[argument.input]);
    } else if ("field" in argument && argument.field in fields) {
      return this.getField(argument, fields[argument.field]);
    } else {
      return "[unknown argument]";
    }
  }

  convertSubstack(input: Sb3BlockInput) {
    let block = this.blocks[input[1]];
    let blockText = this.options.indent + this.convertBlock(block);
    while (block.next !== null) {
      block = this.blocks[block.next];
      blockText +=
        "\n" +
        this.convertBlock(block)
          .split("\n")
          .map((line) => this.options.indent + line)
          .join("\n");
    }
    return blockText;
  }

  convertLayer({
    layer,
    inputs,
    fields,
  }: {
    layer: Layer;
    inputs: Sb3Inputs;
    fields: Sb3Fields;
  }) {
    if ("string" in layer) {
      let layerText = this.translations.translate(layer.string);
      if (layer.arguments) {
        for (let i = 0; i < layer.arguments.length; i++) {
          const argumentText = this.convertArgument({
            argument: layer.arguments[i],
            inputs,
            fields,
          });
          layerText = layerText.replace("%" + (i + 1), argumentText);
        }
      }
      return layerText;
    } else if ("substack" in layer) {
      const inputId = layer.substack;
      const input = inputs[inputId];
      if (input[0] !== 2) throw new Error("Invalid substack input.");
      return this.convertSubstack(input);
    }
  }

  convertBlock(block: Sb3Block): string {
    const blockMeta = blocksMeta.get(block.opcode);

    if (blockMeta) {
      return blockMeta.layers
        .map((layer) => {
          try {
            return this.convertLayer({
              layer,
              inputs: block.inputs,
              fields: block.fields,
            });
          } catch (error) {
            throw new Error(`${error} Block:\n${JSON.stringify(block)}`);
          }
        })
        .join("\n");
    } else {
      let blockText = this.translations.translate(
        block.opcode.toUpperCase().replace(/^OPERATOR_/, "OPERATORS_")
      );
      if (blockText.match(/%/g)?.length === 1) {
        if (Object.values(block.inputs).length) {
          const argContent = this.getInput(
            { input: Object.keys(block.inputs)[0], inputType: "string" },
            Object.values(block.inputs)[0]
          );
          blockText = blockText.replace("%1", argContent);
        } else if (Object.values(block.fields).length) {
          const argContent = this.getField(
            { field: Object.keys(block.inputs)[0] },
            Object.values(block.fields)[0]
          );
          blockText = blockText.replace("%1", argContent);
        }
        return blockText;
      }
      return "unknown block";
    }
  }
}

export const stringFormats = {
  squareBrackets: {
    start: "[",
    end: "]",
  },
  singleQuotes: {
    start: "'",
    end: "'",
  },
  doubleQuotes: {
    start: '"',
    end: '"',
  },
} as const;

export type StringFormat = keyof typeof stringFormats;

import Options, { OptionsArgument } from "./options";
import blocksMeta from "./block-meta";

export default class Translations {
  translations: { [key: string]: string };
  options: Options;

  constructor(
    translations: { [key: string]: string },
    options: OptionsArgument = {}
  ) {
    this.translations = translations;
    this.options = new Options(options);
  }

  translate(string: string): string {
    if (string === "END") {
      return this.options.curlyBracketStacks ? "}" : "end";
    }
    const block = blocksMeta.get(string.toLowerCase());
    if (block && block.type === "stack" && this.options.curlyBracketStacks) {
      return this.translations[string] + " {";
    }
    if (!(string in this.translations))
      throw new Error(`Translation ${string} not found.`);

    return this.translations[string];
  }
}
